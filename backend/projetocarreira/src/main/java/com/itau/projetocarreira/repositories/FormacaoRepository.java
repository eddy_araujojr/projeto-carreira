package com.itau.projetocarreira.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.itau.projetocarreira.models.EnumNivelFormacao;
import com.itau.projetocarreira.models.Formacao;

public interface FormacaoRepository extends CrudRepository<Formacao, Long> {

	public Optional<List<Formacao>> findByCurso(String curso);
	
	public Optional<List<Formacao>> findByNivel(EnumNivelFormacao nivel);
}
