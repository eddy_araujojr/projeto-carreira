package com.itau.projetocarreira.controllers;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.projetocarreira.models.Usuario;
import com.itau.projetocarreira.repositories.UsuarioRepository;
import com.itau.projetocarreira.services.JWTService;
import com.itau.projetocarreira.services.PasswordService;

@RestController
@CrossOrigin
public class LoginController {

	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Autowired
	JWTService jwtService;
	
	@Autowired
	PasswordService passwordService;
	
	@RequestMapping(method=RequestMethod.POST, path="/login")
	public ResponseEntity<?> loginUsuario(@RequestBody Usuario usuario) {
		Optional<Usuario> optionalUsuario = usuarioRepository.findByUsername(usuario.getUsername());
		
		if(!optionalUsuario.isPresent())
			return ResponseEntity.badRequest().build();
		
		Usuario user = optionalUsuario.get();
		
		if (passwordService.verificarHash(usuario.getSenha(), user.getSenha())) {
			String token = jwtService.gerarToken(user.getUsername());
			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", String.format("Bearer %s", token));
			headers.add("Access-Control-Expose-Headers", "Authorization");
			return new ResponseEntity<Usuario>(user, headers, HttpStatus.OK);
		}
		
		return ResponseEntity.badRequest().build();
	}
	
	@RequestMapping(path="/verificar")
	public ResponseEntity<?> verificarToken(HttpServletRequest request){
		String token = request.getHeader("Authorization");
		token = token.replace("Bearer ", "");
		
		String username = jwtService.validarToken(token);
		if(username == null)
			return ResponseEntity.status(403).build();
		
		Optional<Usuario> usuarioOptional = usuarioRepository.findByUsername(username);
		if(!usuarioOptional.isPresent()) 
			return ResponseEntity.status(403).build();
		
		return ResponseEntity.ok().build();
	}
}
