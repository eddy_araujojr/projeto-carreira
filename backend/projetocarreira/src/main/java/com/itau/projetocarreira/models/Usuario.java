package com.itau.projetocarreira.models;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Entity
public class Usuario {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@NotNull
	private String username;
	
	@NotNull
	private String nome;
	
	@NotNull
	private String senha;
	
	@Email
	private String email;
	
	@NotNull
	private LocalDate nascimento;
	
	private EnumNivelFormacao nivel;
	
	@OneToMany(cascade=CascadeType.ALL)
	private List<Formacao> formacoes;
	
	@OneToMany(cascade=CascadeType.ALL)
	private List<Profissao> profissoes;
	
	@OneToMany(cascade=CascadeType.ALL)
	private List<Interesse> idiomas;
	
	@OneToMany(cascade=CascadeType.ALL)
	private List<Interesse> hobbies;
	
	@OneToMany(cascade=CascadeType.ALL)
	private List<Interesse> habitos;
	
	@OneToMany(cascade=CascadeType.ALL)
	private List<Objetivo> objetivos;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public LocalDate getNascimento() {
		return nascimento;
	}

	public void setNascimento(LocalDate nascimento) {
		this.nascimento = nascimento;
	}

	public EnumNivelFormacao getNivel() {
		return nivel;
	}

	public void setNivel(EnumNivelFormacao nivel) {
		this.nivel = nivel;
	}

	public List<Formacao> getFormacoes() {
		return formacoes;
	}

	public void setFormacoes(List<Formacao> formacoes) {
		this.formacoes = formacoes;
	}

	public List<Profissao> getProfissoes() {
		return profissoes;
	}

	public void setProfissoes(List<Profissao> profissoes) {
		this.profissoes = profissoes;
	}

	public List<Interesse> getIdiomas() {
		return idiomas;
	}

	public void setIdiomas(List<Interesse> idiomas) {
		this.idiomas = idiomas;
	}

	public List<Interesse> getHobbies() {
		return hobbies;
	}

	public void setHobbies(List<Interesse> hobbies) {
		this.hobbies = hobbies;
	}

	public List<Interesse> getHabitos() {
		return habitos;
	}

	public void setHabitos(List<Interesse> habitos) {
		this.habitos = habitos;
	}

	public List<Objetivo> getObjetivos() {
		return objetivos;
	}

	public void setObjetivos(List<Objetivo> objetivos) {
		this.objetivos = objetivos;
	}
}
