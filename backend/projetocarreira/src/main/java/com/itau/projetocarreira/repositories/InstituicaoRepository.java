package com.itau.projetocarreira.repositories;

import org.springframework.data.repository.CrudRepository;

import com.itau.projetocarreira.models.Instituicao;

public interface InstituicaoRepository extends CrudRepository<Instituicao, Long> {

}
