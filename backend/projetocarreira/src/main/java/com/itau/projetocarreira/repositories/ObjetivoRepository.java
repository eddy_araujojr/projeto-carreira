package com.itau.projetocarreira.repositories;

import org.springframework.data.repository.CrudRepository;

import com.itau.projetocarreira.models.Objetivo;

public interface ObjetivoRepository extends CrudRepository<Objetivo, Long> {

}
