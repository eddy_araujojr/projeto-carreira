package com.itau.projetocarreira.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.itau.projetocarreira.models.Endereco;

public interface EnderecoRepository extends CrudRepository<Endereco, Long>{
	
	public Optional<Endereco> findByLogradouro(String logradouro);
	
	public Optional<Endereco> findByCep(String cep);
	
	public Optional<Endereco> findByCidade(String cidade);
	

}
