package com.itau.projetocarreira.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.itau.projetocarreira.models.Profissao;

public interface ProfissaoRepository extends CrudRepository<Profissao, Long>{

	public Optional<List<Profissao>> findByFuncao(String funcao);
}
