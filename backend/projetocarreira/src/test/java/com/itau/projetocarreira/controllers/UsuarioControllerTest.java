package com.itau.projetocarreira.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;
import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.itau.projetocarreira.models.EnumNivelFormacao;
import com.itau.projetocarreira.models.Formacao;
import com.itau.projetocarreira.models.Interesse;
import com.itau.projetocarreira.models.Objetivo;
import com.itau.projetocarreira.models.Profissao;
import com.itau.projetocarreira.models.Usuario;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class UsuarioControllerTest {

	@Autowired
	UsuarioController controller;
	
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void retornaUsuarioCadastrado() throws Exception{
        String url = String.format("http://localhost:%s/usuarios", port);
        ResponseEntity<Usuario> resposta = restTemplate.postForEntity(url, usuarioTeste(), Usuario.class);

        
        assertNotNull(resposta.getBody());
    }
    
    public Usuario usuarioTeste() {
    	Usuario user = new Usuario();
		user.setId(1);
		user.setNome("João Neves");
		user.setUsername("jonsnow");
		user.setSenha("inverno123");
		user.setEmail("joao@inverno.com");
		user.setNascimento(LocalDate.of(1989, 01, 01));
		user.setNivel(EnumNivelFormacao.BACHARELADO);
		
		Formacao form = new Formacao();
		/*form.setId(1);
		form.setCurso("Matematica");
		form.setNivel(EnumNivelFormacao.BACHARELADO);
		form.setDuracao(48);
		form.setDataConclusao(LocalDate.of(2017, 12, 01));
		form.setCompleto(true);*/
		
		user.setFormacoes(new ArrayList<Formacao>());
		//user.getFormacoes().add(form);
		
		//Profissao prof = new Profissao();
		
		user.setProfissoes(new ArrayList<Profissao>());
		//user.getProfissoes().add(new Profissao());
		
		/*user.setIdiomas(new ArrayList<Interesse>());
		user.getIdiomas().add(new Interesse());
		user.setHobbies(new ArrayList<Interesse>());
		user.getHobbies().add(new Interesse());
		user.setHabitos(new ArrayList<Interesse>());
		user.getHabitos().add(new Interesse());
		
		user.setObjetivos(new ArrayList<Objetivo>());
		user.getObjetivos().add(new Objetivo());*/
		
		return user;
    }
}
