package com.itau.projetocarreira.models;

import java.time.LocalDate;
import org.junit.Test;
import com.itau.projetocarreira.models.EnumCargo;
import static org.junit.Assert.assertEquals;



public class ProfissaoTest {

	@Test
	public void profissaoUm () {
		Profissao profissao = new Profissao();
		profissao.setId(1);
		profissao.setFuncao("Administrador");
		profissao.setCargo(EnumCargo.JUNIOR);
		profissao.setSalario(3000);
		profissao.setInicio(LocalDate.of(2016,1,1));
		profissao.setFim(LocalDate.of(2018,1,1));
		Instituicao empresa = new Instituicao();
		empresa.setId(1);
		profissao.setEmpresa(empresa);
		
		assertEquals(1, profissao.getId());
		assertEquals("Administrador", profissao.getFuncao());
		assertEquals(EnumCargo.JUNIOR, profissao.getCargo());
		assertEquals(3000, profissao.getSalario(), 0.01);
		assertEquals(LocalDate.of(2016,1,1), profissao.getInicio());
		assertEquals(LocalDate.of(2018,1,1), profissao.getFim());
		assertEquals(empresa, profissao.getEmpresa());
	}
}
