package com.itau.projetocarreira.models;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ObjetivoTest {
	
	@Test
	public void objetivoUm() {
		Objetivo objetivo = new Objetivo();
		objetivo.setId(1);
		objetivo.setDescricao("Tentar dominar o mundo");
		objetivo.setPrazo(24);
		
		assertEquals(1, objetivo.getId());
		assertEquals("Tentar dominar o mundo", objetivo.getDescricao());
		assertEquals(24, objetivo.getPrazo());
	}

}
