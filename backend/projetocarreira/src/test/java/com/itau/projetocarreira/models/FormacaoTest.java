package com.itau.projetocarreira.models;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Test;

public class FormacaoTest {

	@Test
	public void formacaoUm() {
		Formacao formacao = new Formacao();
		formacao.setId(1);
		formacao.setCurso("Matemática");
		formacao.setDataConclusao(LocalDate.of(2017, 12, 1));
		formacao.setDuracao(48);
		formacao.setCompleto(true);
		formacao.setNivel(EnumNivelFormacao.BACHARELADO);
		Instituicao instituicao = new Instituicao();
		instituicao.setId(1);
		formacao.setInstituicao(instituicao);
		
		assertEquals(1, formacao.getId());
		assertEquals("Matemática", formacao.getCurso());
		assertEquals(LocalDate.of(2017, 12, 1), formacao.getDataConclusao());
		assertEquals(48, formacao.getDuracao());
		assertEquals(true, formacao.isCompleto());
		assertEquals(EnumNivelFormacao.BACHARELADO, formacao.getNivel());
		assertEquals(instituicao, formacao.getInstituicao());
	}
}
