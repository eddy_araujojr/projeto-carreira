package com.itau.projetocarreira.models;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.util.ArrayList;

import org.junit.Test;


public class UsuarioTest {

	@Test
	public void usuarioUm() {
		Usuario user = new Usuario();
		user.setId(1);
		user.setNome("João Neves");
		user.setUsername("jonsnow");
		user.setSenha("inverno123");
		user.setEmail("joao@inverno.com");
		user.setNascimento(LocalDate.of(1989, 01, 01));
		user.setNivel(EnumNivelFormacao.BACHARELADO);
		
		Formacao form = new Formacao();
		form.setId(1);
		form.setCurso("Matematica");
		form.setNivel(EnumNivelFormacao.BACHARELADO);
		form.setDuracao(48);
		form.setDataConclusao(LocalDate.of(2017, 12, 01));
		form.setCompleto(true);
		
		user.setFormacoes(new ArrayList<Formacao>());
		user.getFormacoes().add(form);
		
		user.setProfissoes(new ArrayList<Profissao>());
		user.getProfissoes().add(new Profissao());
		
		user.setIdiomas(new ArrayList<Interesse>());
		user.getIdiomas().add(new Interesse());
		user.setHobbies(new ArrayList<Interesse>());
		user.getHobbies().add(new Interesse());
		user.setHabitos(new ArrayList<Interesse>());
		user.getHabitos().add(new Interesse());
		
		user.setObjetivos(new ArrayList<Objetivo>());
		user.getObjetivos().add(new Objetivo());
		
		assertEquals(user.getId(), 1);
		assertEquals(user.getNome(), "João Neves");
		assertEquals(user.getUsername(), "jonsnow");
		assertEquals(user.getSenha(), "inverno123");
		assertEquals(user.getEmail(), "joao@inverno.com");
		assertEquals(user.getNascimento(), LocalDate.of(1989, 01, 01));
		assertEquals(user.getNivel(), EnumNivelFormacao.BACHARELADO);
	}
	
}
