const urlUsuarios = "http://localhost:8080/usuarios/";

let usuario;
let itensgridpessoas = 1;
let restrictedIds;

function carregarPerfil() {
    if (localStorage.getItem("token") == null) {
        window.location.href = "../index.html";
    }
    let userid = localStorage.getItem("userid");
    restrictedIds = [userid];
    
    let getmontado = montarGet();
    fetch(urlUsuarios+userid, getmontado).then(extrairJSON).then(tratarRetornoCarregarPerfil).catch(tratarErro);
}

function tratarRetornoCarregarPerfil(retorno) {
    console.log(retorno);
    usuario = retorno;
    let headerOla = document.createElement("h3");
    headerOla.innerHTML = "Olá, " + retorno.nome;
    sct_ola.appendChild(headerOla);
    carregarPessoasProfissao();
}

function carregarPessoasProfissao() {
    console.log("carregarPessoasProfissao");
    console.log(usuario.profissoes.length);
    if(usuario.profissoes.length > 0) {
        let url = urlUsuarios+"profissao/"+usuario.profissoes[0].funcao;
        console.log(url);
        fetch(url, montarGet()).then(extrairJSON).then(tratarRetornoPessoasProfissao).catch(tratarErro);
    }
}

function tratarRetornoPessoasProfissao(retorno) {
    console.log(retorno);
    for (let index = 0; index < retorno.length || itensgridpessoas === 5; index++) {
        let restrictedid = false;
        for (let index2 = 0; index2 < restrictedIds.length; index2++) {
            if(retorno[index].id == restrictedIds[index2]) 
                restrictedid = true;
        }
        if(!restrictedid) {
            console.log("vai");
            let usuarioIndex = retorno[index];
            restrictedIds.push(usuarioIndex.id);
            let hPessoas = document.createElement("h3");
            hPessoas.innerHTML = (itensgridpessoas) + " - " + usuarioIndex.nome;
            div_pessoas.appendChild(hPessoas);

            let informacoes = usuarioIndex.nascimento + " / " + usuarioIndex.nivel;
            if(usuarioIndex.profissoes.length > 0){
                informacoes = informacoes + " / " + usuarioIndex.profissoes[0].funcao + " / " + usuarioIndex.profissoes[0].cargo;
            }

            let pPessoas = document.createElement("p");
            pPessoas.innerHTML = informacoes;
            div_pessoas.appendChild(pPessoas);
            itensgridpessoas++;
        }
    }
    carregarPessoasFormacao();
}

function carregarPessoasFormacao() {
    console.log("carregarPessoasFormacao");
    console.log(usuario.formacoes.length);
    if(usuario.profissoes.length > 0) {
        let url = urlUsuarios+"formacao/"+usuario.formacoes[0].curso;
        console.log(url);
        fetch(url, montarGet()).then(extrairJSON).then(tratarRetornoPessoasFormacao).catch(tratarErro);
    }
}

function tratarRetornoPessoasFormacao(retorno) {
    console.log(retorno);
    for (let index = 0; index < retorno.length || itensgridpessoas === 5; index++) {
        let restrictedid = false;
        for (let index2 = 0; index2 < restrictedIds.length; index2++) {
            console.log(retorno[index].id + " | " + restrictedIds[index2]);
            if(retorno[index].id == restrictedIds[index2])
                restrictedid = true;
        }
        if(!restrictedid) {
            let usuarioIndex = retorno[index];
            restrictedIds.push(usuarioIndex.id);
            let hPessoas = document.createElement("h3");
            hPessoas.innerHTML = (itensgridpessoas) + " - " + usuarioIndex.nome;
            div_pessoas.appendChild(hPessoas);

            let informacoes = usuarioIndex.nascimento + " / " + usuarioIndex.nivel;
            if(usuarioIndex.profissoes.length > 0){
                informacoes = informacoes + " / " + usuarioIndex.formacoes[0].curso + " / " + usuarioIndex.formacoes[0].instituicao.nome;
            }
            let pPessoas = document.createElement("p");
            pPessoas.innerHTML = informacoes;
            div_pessoas.appendChild(pPessoas);
            itensgridpessoas++;
        }
    }
}


function tratarErro(params) {
    alert("Sistema indisponível no momento. Por favor, tente novamente mais tarde.");
    console.log("Sistema indisponível no momento. Por favor, tente novamente mais tarde. - " + params);
}

function extrairJSON(resposta) {
    console.log(resposta.status);
    if(resposta.status === 200){
        return resposta.json();
    }
    else {
        console.log(resposta);
    }
}

function montarGet(){
    let token = localStorage.getItem("token");
    return {
        method: 'GET',
        headers: {
            'Content-type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Authorization': token
        }
    };
}

window.onload = carregarPerfil;