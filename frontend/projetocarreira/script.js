
const pagina = document.querySelector("body");
const links = document.querySelectorAll("a");

let cor_texto = "darkblue";

function validarEmail() {
    let email = inp_email.value;
    let arroba = email.indexOf("@");
    let ultimoArroba = email.lastIndexOf("@");
    let ultimoPonto = email.lastIndexOf(".");
    if (email !== "" && (arroba !== ultimoArroba || arroba === -1 ||
        ultimoPonto - arroba < 3)) {
        return false;
    }
    return true;
}

function validarSenha() {
    let senha = inp_senha.value.toUpperCase();
    let numeros = /[0-9]/g;
    let letras = /[A-Z]/g;
    let retorno = false;

    let tamanho = senha.length > 5;
    let temNumero = senha.match(numeros) !== null;
    let temLetras = senha.match(letras) !== null;

    retorno = (tamanho && temNumero && temLetras) || senha === "";

    console.log(senha + " - " + retorno);
    return retorno;
}

function eventoEmail() {
    if (validarEmail()) {
        lbl_email.style.display = "none";
    }
    else {
        lbl_email.style.display = "block";
    }
}

function eventoSenha() {
    if (validarSenha()) {
        lbl_senha.style.color = "transparent";
    }
    else {
        lbl_senha.style.color = "red";
    }
}

function linksAzuis() {
    //for (let link of links) {
        //link.style.color = cor_texto;
    //}
    //border-left: 1px solid steelblue;
    //border-right: 1px solid steelblue;
    this.style.color = cor_texto;
    if (cor_texto === "darkblue") {
        cor_texto = "steelblue";
        this.style.borderLeft = "1px solid steelblue";
        this.style.borderRight = "1px solid steelblue";
    }
    else {
        cor_texto = "darkblue";
        this.style.borderLeft = "1px solid transparent";
        this.style.borderRight = "1px solid transparent";
    }
}

function realizarLogin(dados) {
    let usuario = {
        username: inp_email.value,
        senha: inp_senha.value
    };
    let postmontado = montarPost(usuario);
    console.log(JSON.stringify(postmontado));
    fetch("http://localhost:8080/login", postmontado).then(extrairJSON).then(retornoLogin).catch(tratarErro);
}

function retornoLogin(retorno) {
    console.log(retorno);
    localStorage.setItem("userid", retorno.id);
    window.location.href = "perfil/index.html";
}

function tratarErro(params) {
    alert("Sistema indisponível no momento. Por favor, tente novamente mais tarde.");
    console.log("Problemas ao realizar login - " + params);
}

function extrairJSON(resposta) {
    console.log(resposta.status);
    if(resposta.status === 200){
        alert("Login realizado com sucesso");
        localStorage.setItem("username", inp_email.value);
        localStorage.setItem("token", resposta.headers.get("Authorization"));
        return resposta.json();
    }
    else {
        alert("Não foi possível realizar o login");
        return resposta.json(); 
    }
}

function montarPost(data){
    return {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        }
    };
}

function montarGet(data){
    return {
        method: 'GET',
        headers: {
            'Content-type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        }
    };
}

function toCadastrar() {
    location.href = "cadastro/cadastro.html";
}

for (let link of links) {
    link.onmouseenter = linksAzuis;
    link.onmouseleave = linksAzuis;
}

//inp_email.onblur = eventoEmail;
inp_senha.onblur = eventoSenha;

btn_logon.onclick = realizarLogin;
btn_cadastrar.onclick = toCadastrar;