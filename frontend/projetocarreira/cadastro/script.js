

function validarEmail() {
    let email = inp_email.value;
    let arroba = email.indexOf("@");
    let ultimoArroba = email.lastIndexOf("@");
    let ultimoPonto = email.lastIndexOf(".");
    if (email !== "" && (arroba !== ultimoArroba || arroba === -1 ||
        ultimoPonto - arroba < 3)) {
        return false;
    }
    return true;
}

function eventoEmail() {
    if (validarEmail()) {
        lbl_email.style.color = "transparent";
    }
    else {
        lbl_email.style.color = "red"; 
    }
}

function montarUsuario() {
    return {
        nome: inp_nome.value,
        username: inp_username.value,
        email: inp_email.value,
        senha: inp_senha.value,
        nascimento: inp_dtnascimento.value,
        nivel: slt_nivel.value,
        formacoes: [
            {
                nivel: slt_nivel.value,
                instituicao:  {
                    nome: inp_instituicao.value,
                    enderecos: [
                        {
                            logradouro: "Avenida Tal",
                            numero: 12,
                            cep: "12345-999",
                            cidade: "São Paulo",
                            estado: "SP",
                            pais: "Brasil"
                        }
                    ]
                },
                curso: inp_curso.value,
                isCompleto: inp_iscompleto.value,
                duracao: inp_duracao.value,
                dataConclusao: inp_dtconclusao.value
            }
         ],
        profissoes:[
            {
                cargo: slt_cargo.value,
                funcao: inp_profissao.value,
                salario: inp_salario.value,
                empresa: {
                    nome: inp_empresa.value,
                    enderecos: [
                        {
                            logradouro: "Avenida Tals",
                            numero: 13,
                            cep: "12345-999",
                            cidade: "São Paulo",
                            estado: "SP",
                            pais: "Brasil"
                        }
                    ]
                },
                inicio: inp_dtcargoini.value
            }
        ],
        idiomas:[
            {
                tipo: "idioma",
                descricao: inp_idiomas.value
            }
        ],
        hobbies:[
            {
                tipo: "hobbies",
                descricao: inp_hobbies.value
            }
        ],
        habitos:[
            {
                tipo: "habitos",
                descricao: inp_habitos.value
            }
        ],
        objetivos:[
            {
                descricao: inp_objetivo.value,
                prazo: inp_prazo.value    
            }
        ]
    }
}

function realizarCadastro(dados) {
    let usuario = montarUsuario();
    let postmontado = montarPost(usuario);
    console.log(JSON.stringify(postmontado));
    fetch("http://localhost:8080/usuarios", postmontado).then(extrairJSON).then(retornoCadastro).catch(tratarErro);
    //fetch("http://api.icndb.com/jokes/random").then(extrairJSON).then(retornoLogin);
}

function retornoCadastro(retorno) {
    console.log(retorno);
    location.href = "../index.html";
    //console.log(retorno.status);
}

function extrairJSON(resposta) {
    console.log(resposta.status);
    if(resposta.status === 200){
        alert("Login criado com sucesso");
        return resposta.json();
    }
    alert("Problemas ao criar o usuário");
    return resposta.json();
}

function tratarErro(params) {
    alert("O sistema não conseguiu processar seu cadastro por completo. Por favor, tente realizar o login e, se falhar, tente o cadastro novamente.");
    console.log("Erro ao cadastrar - " + params);
}

function montarPost(data){
    return {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        }
    };
}

inp_email.onblur = eventoEmail;
btn_finalizar.onclick = realizarCadastro;